(function(){
let fecha = document.getElementById(".fecha");
let actualizarHora=function() {
    let fecha = new Date(),
        horas = fecha.getHours(),
        ampm,

    minutos = fecha.getMinutes(),
    segundos = fecha.getSeconds(),
    diaSemana = fecha.getDay(),
    dia = fecha.getDate(),
    mes = fecha.getMonth(),
    anio = fecha.getFullYear();  

    let horasP = document.getElementById('horas'), 
    ampmP = document.getElementById('ampm'),
    minutosP = document.getElementById('minutos'),
    segundosP = document.getElementById('segundos'),
    diaSemanaP = document.getElementById('diaSem'),
    diaP = document.getElementById('dia'),
    mesP = document.getElementById('mes'),
    anioP = document.getElementById('anio');

    let semana = ['domingo','lunes','martes','miercoles','juevez', 'viernes', 'sabado'];
    diaSemanaP.textContent = semana[diaSemana];

    diaP.textContent=dia;

    let mesesDelanio= ['enero', 'febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];
    mesP.textContent = mesesDelanio[mes];

    anioP.textContent = anio;

    if (horas>=12) {
       
        ampm='PM'
    } else {
        ampm='AM'
    }
    if (horas == 0) {
        horas =12;
    }
    if (minutos<=9){
        minutos = "0"+minutos;
    }

    if (segundos<=10){
        segundos="0"+segundos;
    }
    horasP.textContent = horas;
    minutosP.textContent = minutos;
    segundosP.textContent = segundos;
    ampmP.textContent = ampm;
    if (horas>=18 && ampm=="PM") {
        document.body.style.backgroundImage ='url(https://www.lostiempos.com/sites/default/files/media_imagen/2017/9/27/ciudad-de-cochabamba-atardecer.jpg)';
        document.body.style.color='white';
       }
};
 
actualizarHora();
let actualizar = setInterval(actualizarHora,1000)
}())